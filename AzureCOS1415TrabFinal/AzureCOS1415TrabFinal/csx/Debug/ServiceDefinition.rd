﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="AzureCOS1415TrabFinal" generation="1" functional="0" release="0" Id="18244863-f642-48e2-977f-b3c8079c365b" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="AzureCOS1415TrabFinalGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="WebRoleBank:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/LB:WebRoleBank:Endpoint1" />
          </inToChannel>
        </inPort>
        <inPort name="WebRoleMyBroker:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/LB:WebRoleMyBroker:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="WebRoleBank:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/MapWebRoleBank:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebRoleBank:StorageConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/MapWebRoleBank:StorageConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebRoleBankInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/MapWebRoleBankInstances" />
          </maps>
        </aCS>
        <aCS name="WebRoleMyBroker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/MapWebRoleMyBroker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebRoleMyBrokerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/MapWebRoleMyBrokerInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:WebRoleBank:Endpoint1">
          <toPorts>
            <inPortMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBank/Endpoint1" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:WebRoleMyBroker:Endpoint1">
          <toPorts>
            <inPortMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleMyBroker/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapWebRoleBank:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBank/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWebRoleBank:StorageConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBank/StorageConnectionString" />
          </setting>
        </map>
        <map name="MapWebRoleBankInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBankInstances" />
          </setting>
        </map>
        <map name="MapWebRoleMyBroker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleMyBroker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWebRoleMyBrokerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleMyBrokerInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="WebRoleBank" generation="1" functional="0" release="0" software="D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\AzureCOS1415TrabFinal\csx\Debug\roles\WebRoleBank" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="8080" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="StorageConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WebRoleBank&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;WebRoleBank&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebRoleMyBroker&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBankInstances" />
            <sCSPolicyUpdateDomainMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBankUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBankFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="WebRoleMyBroker" generation="1" functional="0" release="0" software="D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\AzureCOS1415TrabFinal\csx\Debug\roles\WebRoleMyBroker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WebRoleMyBroker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;WebRoleBank&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebRoleMyBroker&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleMyBrokerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleMyBrokerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleMyBrokerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WebRoleMyBrokerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="WebRoleBankUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="WebRoleBankFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="WebRoleMyBrokerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="WebRoleBankInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="WebRoleMyBrokerInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="d19bfca2-b8f0-4bf4-b611-475a45019091" ref="Microsoft.RedDog.Contract\ServiceContract\AzureCOS1415TrabFinalContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="1b3ebbb5-89ee-4f69-b8c9-edb26d58dd84" ref="Microsoft.RedDog.Contract\Interface\WebRoleBank:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleBank:Endpoint1" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="b847d71c-0b68-45ed-a201-953fd12f2c39" ref="Microsoft.RedDog.Contract\Interface\WebRoleMyBroker:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/AzureCOS1415TrabFinal/AzureCOS1415TrabFinalGroup/WebRoleMyBroker:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>