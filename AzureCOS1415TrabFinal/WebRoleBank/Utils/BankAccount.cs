﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebRoleBank.Utils
{
    public class BankAccount
    {

         // Variáveis de Classe
        public int accountNumber { get; set; }
        public double balance { get; set; }


         //Outras variáveis
         private static Object jobLock = new Object();


            // Contrutor
            public BankAccount() { }

            // Contrutor
            public BankAccount(int accountNr)
            {
                this.accountNumber = accountNr;
            }


            // Contrutor
            public BankAccount(int accountNr, double bal)
            {
                this.accountNumber = accountNr;
                this.balance = bal;
            }
            


            


            //Método para criar conta bancária (recebe número de conta e saldo inicial)
            public void CreateAccount(double initialBALANCE, string fstName, string lstName)
            {

                int nextAccountNumber = SQLUtils.SQLCheckNextAccountNumber("SELECT COUNT(*) FROM Account");

                SQLUtils.SQLCreateAccount("INSERT INTO Account (accountNumber,balance,firstName,lastName) VALUES (" +
                  + nextAccountNumber + "," + initialBALANCE + ",'" + fstName + "','" + lstName + "')");
            }



            // Método para depositar um montante na conta bancária
            public void Deposit (int accountNr, double amount)
            {

           
                BankAccount b = new BankAccount(accountNr, amount);

                var result = SQLUtils.SQLselectCommand("SELECT balance FROM Account WHERE accountNumber = '" + accountNr + "'");

                lock(jobLock)
                {                    
                    if (result == "")
                    {
                        Console.WriteLine("Conta bancária não existe");
                    }
                    else if (result != "")
                    {

                        b.balance = Convert.ToDouble(SQLUtils.SQLselectCommand("SELECT balance FROM Account WHERE accountNumber = '" + accountNr + "'"));

                        b.balance += amount;

                        SQLUtils.SQLCreateAccount("UPDATE Account SET balance=" + b.balance + " WHERE accountNumber=" + accountNr);
                    }
                }

                Console.WriteLine("O saldo foi atualizado para :{0}", b.balance.ToString());
            }


            // Método para efetuar levantamento de um montante na conta bancária
            public void Withdraw(int accountNr, double amount)
            {

          
                BankAccount b = new BankAccount(accountNr, amount);

                lock (jobLock)
                {

                    var result = SQLUtils.SQLselectCommand("SELECT balance FROM Account WHERE accountNumber = '" + accountNr + "'");

                    if (result == "")
                    {
                        Console.WriteLine("Conta bancária não existe");
                    }
                    else if (result != "")
                    {
                
                        b.balance = Convert.ToDouble(SQLUtils.SQLselectCommand("SELECT balance FROM Account WHERE accountNumber = '" + accountNr + "'"));
       
                        b.balance -= amount;

                        SQLUtils.SQLCreateAccount("UPDATE Account SET balance=" + b.balance + " WHERE accountNumber=" + accountNr);                 
                    }

                }

                Console.WriteLine("O saldo foi atualizado para :{0}", b.balance.ToString());
            }
   
           
            // Método para verificar o saldo da conta bancária
            public string GetAccountBalance(int accountNr)
            {       
                  string result = "";  
                  var query = SQLUtils.SQLselectCommand("SELECT balance FROM Account WHERE accountNumber='" + accountNr +"'");

                if (query == "")
                {
                    result = "A conta que forneceu não existe";
                }
                else if (query != "")
                {
                    result = query;
                }

                return result;
            }


            // Método que verifica se existe saldo para concretizar operação
            public bool IsTransferAuthorized(int accNumber, double transferAmount)
            {

                this.accountNumber = accNumber;

                BankAccount b = new BankAccount(accNumber);

                lock (jobLock)
                {
                    if (transferAmount > b.balance)
                    {

                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }

            }
    }
}