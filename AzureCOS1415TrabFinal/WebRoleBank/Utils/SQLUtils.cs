﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebRoleBank.Utils
{
    public class SQLUtils
    {


        //Método de conexão à BD - Verifica qual é o proximo número de conta bancária disponível
        public static int SQLCheckNextAccountNumber(string command)
        {
         
            int nextAvailableAccountNr = 0;

            using(SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleBank\App_Data\BankAccounts.mdf;Integrated Security=True"))
            {
                try
                {
                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);
                    nextAvailableAccountNr = (int)myCommand.ExecuteScalar();                 
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Excepção: {0}", ex.ToString());
                }

                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return nextAvailableAccountNr + 1;
        }

        //Método de conexão à BD - Criar Conta Bancária
        public static void SQLCreateAccount(string command)
        {

            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleBank\App_Data\BankAccounts.mdf;Integrated Security=True"))
            {
                try
                {

                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);
                    myCommand.ExecuteNonQuery();
                    conn.Close();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Dispose();
                }
            }

        }

        //Método de conexão à BD - Executa DataReader
        public static string SQLselectCommand(string command)
        {


            string bal = "";
            SqlDataReader myReader;

            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleBank\App_Data\BankAccounts.mdf;Integrated Security=True"))
            {
                try
                {
                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {
                        bal = (myReader["balance"].ToString());
                    }

                    conn.Close();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Dispose();
                }

            }
            return bal;
        }



        //Método de conexão à BD - Executa DataReader
        public static void SQLUpdateCommand(string command)
        {

            using (  SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleBank\App_Data\BankAccounts.mdf;Integrated Security=True"))
            {
                try
                {
                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);
                    myCommand.ExecuteNonQuery();
                    conn.Close();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Dispose();
                }
            }


        }
    }
}