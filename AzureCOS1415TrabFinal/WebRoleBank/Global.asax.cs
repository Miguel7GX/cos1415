﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace WebRoleBank
{

    public class CustomerEntity : TableEntity
    {

        //public CustomerEntity(string accountNumber, string balance)
        public CustomerEntity(int accountNumber, double balance)
        {

            this.PartitionKey = Convert.ToString(accountNumber);
            this.RowKey = Convert.ToString(balance);
        }


        public CustomerEntity() { }


        public int accountNumber { get; set; }
        public double balance { get; set; }
    }


    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            try
            {
                // Azure StorageAccount - "StorageConnectionString"
               // CloudStorageAccount storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString"));
               // CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
                
                // Cria um Cliente da table 
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

                // Cria uma table (se esta não existir)
                CloudTable table = tableClient.GetTableReference("accounts");
                table.CreateIfNotExists();

                
            }

            catch (Exception ex)
            {
                Console.WriteLine("Exception generated: {0}", ex.ToString());
            }
        }


        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }
    }
}