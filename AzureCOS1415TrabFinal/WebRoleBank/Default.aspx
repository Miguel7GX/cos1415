﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebRoleBank._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
<%--                <table style="width: 100%;">

                <tr>
                    <td style="width: 242px">

               <h2>My Bank Gmbh</h2>

                    </td>

            </tr>
        </table>--%>
        <h2>My Bank Gmbh</h2>
        <p class="lead">
                        <asp:Image ID="ImageLogo" runat="server" Height="163px" ImageUrl="~/Pics/Pic_Default.jpg" Width="229px"/>
                    </p>
                <p>&nbsp;</p>
                <p>Operações bancárias disponíveis:</p>
                <p><asp:Image ID="Image_OpenAccount" runat="server" Height="130px" ImageUrl="~/Pics/Open_a_Bank_account.png" Width="160px" />
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="Image_Deposit" runat="server" Height="130px" ImageUrl="~/Pics/piggy_bank_deposit.jpg" Width="160px" />
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="Image_Withdraw" runat="server" Height="130px" Width="160px" ImageUrl="~/Pics/withdraw.jpg" />
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="Image_CheckBalance" runat="server" Height="130px" ImageUrl="~/Pics/bank-balance.jpg" Width="160px" />
&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:Image ID="Image_AllAccounts" runat="server" Height="130px" ImageUrl="~/Pics/bank_accounts.jpg" Width="160px" />
                </p>
                <p><asp:Button ID="Button_CreateAccount" runat="server" Text="Criar Conta" OnClick="ButtonCreateAccount_Click" Width="160px" />
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonDeposit" runat="server" OnClick="ButtonDeposit_Click" Text="Depositar" Width="160px" />
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonWithdraw" runat="server" Text="Levantar" Width="160px" OnClick="ButtonWithdraw_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="ButtonCheckBalance" runat="server" OnClick="ButtonCheckBalance_Click" Text="Ver Saldo" Width="160px" />
&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="ButtonGetAllAccounts"  runat="server" Text="Ver Contas" Width="160px" OnClick="ButtonGetAllAccounts_Click" />
                </p>
    </div>

</asp:Content>
