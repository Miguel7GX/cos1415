﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebRoleBank
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        protected void ButtonCreateAccount_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Operations/CreateAccount.aspx");
        }

        protected void ButtonCheckBalance_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Operations/CheckBalance.aspx");
        }

        protected void ButtonDeposit_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Operations/Deposit.aspx");
        }

        protected void ButtonWithdraw_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Operations/Withdraw.aspx");
        }

        protected void ButtonGetAllAccounts_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Operations/GetAllAccounts.aspx");
        }
    }
}