﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebRoleBank.Startup))]
namespace WebRoleBank
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
