﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebRoleBank
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_CreateAccount_Click(object sender, EventArgs e)
        {
            
            WebRoleBank.Utils.BankAccount b = new Utils.BankAccount();

            try
            {           
                b.CreateAccount(Convert.ToDouble(TextBox_initialBalance.Text), TextBox_firstName.Text, TextBox_lastName.Text);

                LabelAccountNr.Text = "Foi criada uma nova conta bancária com o número " + (Convert.ToString(Utils.SQLUtils.SQLCheckNextAccountNumber("SELECT COUNT(*) FROM Account") - 1));
                LabelBalance.Text = "e com saldo " + TextBox_initialBalance.Text;
            }

            catch (Exception ex)
            {
                Console.WriteLine("Excepção gerada {0}: ", ex.ToString());
            }
        }
    }
}