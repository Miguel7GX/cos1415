﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GetAllAccounts.aspx.cs" Inherits="WebRoleBank.Operations.GetAllAccounts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>

        Resumo de todas as contas bancárias existentes no My Bank:<p>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGetAllAccounts" Height="155px" Width="681px">
        <Columns>
            <asp:BoundField DataField="Nr_Conta" HeaderText="Nr_Conta" SortExpression="Nr_Conta" />
            <asp:BoundField DataField="Saldo" HeaderText="Saldo" SortExpression="Saldo" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
            <asp:BoundField DataField="Apelido" HeaderText="Apelido" SortExpression="Apelido" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSourceGetAllAccounts" runat="server" ConnectionString="<%$ ConnectionStrings:GetAllAccountsConnectionString %>" SelectCommand="SELECT        accountNumber AS Nr_Conta, balance AS Saldo, firstName AS Nome, lastName AS Apelido
FROM            Account"></asp:SqlDataSource>


</asp:Content>
