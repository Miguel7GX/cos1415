﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebRoleBank
{
    public partial class Deposit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonDepositar_Click(object sender, EventArgs e)
        {

            WebRoleBank.Utils.BankAccount b = new Utils.BankAccount();
            b.Deposit(Convert.ToInt32(TextBox_AccountNr.Text), Convert.ToDouble(TextBox_Amount.Text));
        }
    }
}