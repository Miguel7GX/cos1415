﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebRoleBank
{
    /// <summary>
    /// Summary description for BankService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BankService : System.Web.Services.WebService
    {


        [WebMethod]
        public double ReturnAccountBalance(int accountNr)
        {
            double accountBalance = 0;

            try
            {
                accountBalance = Convert.ToDouble(Utils.SQLUtils.SQLselectCommand("SELECT balance FROM Account WHERE accountNumber = '" + accountNr + "'"));
            }

            catch
            {

            }

            return accountBalance;
        }




        protected static double GetBalance(int accountNr)
        {
            string accNr = Convert.ToString(accountNr);
            double result = 0.0;

            string balance = Utils.SQLUtils.SQLselectCommand("SELECT balance FROM Account WHERE accountNumber=" + accNr + "");

            if(balance != "")
            {
                result = double.Parse(balance);
            }
            

            return result;
        }


        [WebMethod]
        public bool AuthorizeOperation(int accountNr, double requestedAmmount)
        {

            double accountBal = GetBalance(accountNr);

            return (GetBalance(accountNr) > requestedAmmount);
        }


        [WebMethod]
        public bool doesBankAccountExist(int accountNr)
        {
            bool bankAccountExist = false;

            double bal = GetBalance(accountNr);

            if (bal != 0.0)
            {
                bankAccountExist.Equals(true);
            }

            return bankAccountExist;
        }


        [WebMethod]
        public void Withdraw(int accountNr, double ammount)
        {

            WebRoleBank.Utils.BankAccount b = new Utils.BankAccount();
            b.Withdraw(accountNr, ammount);
        }

        [WebMethod]
        public void Deposit(int accountNr, double ammount)
        {

            WebRoleBank.Utils.BankAccount b = new Utils.BankAccount();
            b.Deposit(accountNr, ammount);
        }
    }
}
