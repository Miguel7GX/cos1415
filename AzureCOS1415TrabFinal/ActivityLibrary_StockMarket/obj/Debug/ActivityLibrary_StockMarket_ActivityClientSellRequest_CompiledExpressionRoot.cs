namespace ActivityLibrary_StockMarket {
    
    #line 24 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Collections;
    
    #line default
    #line hidden
    
    #line 25 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Collections.Generic;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Activities;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Activities.Expressions;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Activities.Statements;
    
    #line default
    #line hidden
    
    #line 26 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Data;
    
    #line default
    #line hidden
    
    #line 27 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Linq;
    
    #line default
    #line hidden
    
    #line 28 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Text;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\ActivityLibrary_StockMarket\ActivityClientSellRequest.xaml"
    using System.Activities.XamlIntegration;
    
    #line default
    #line hidden
    
    
    public partial class ActivityClientSellRequest : System.Activities.XamlIntegration.ICompiledExpressionRoot {
        
        private System.Activities.Activity rootActivity;
        
        private object dataContextActivities;
        
        private bool forImplementation = true;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public string GetLanguage() {
            return "C#";
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public object InvokeExpression(int expressionId, System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext) {
            if ((this.rootActivity == null)) {
                this.rootActivity = this;
            }
            if ((this.dataContextActivities == null)) {
                this.dataContextActivities = ActivityClientSellRequest_TypedDataContext1_ForReadOnly.GetDataContextActivitiesHelper(this.rootActivity, this.forImplementation);
            }
            if ((expressionId == 0)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityClientSellRequest_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext0 = ((ActivityClientSellRequest_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext0.ValueType___Expr0Get();
            }
            if ((expressionId == 1)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityClientSellRequest_TypedDataContext1.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[1] == null)) {
                    cachedCompiledDataContext[1] = new ActivityClientSellRequest_TypedDataContext1(locations, activityContext, true);
                }
                ActivityClientSellRequest_TypedDataContext1 refDataContext1 = ((ActivityClientSellRequest_TypedDataContext1)(cachedCompiledDataContext[1]));
                return refDataContext1.GetLocation<string>(refDataContext1.ValueType___Expr1Get, refDataContext1.ValueType___Expr1Set, expressionId, this.rootActivity, activityContext);
            }
            if ((expressionId == 2)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityClientSellRequest_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext2 = ((ActivityClientSellRequest_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext2.ValueType___Expr2Get();
            }
            if ((expressionId == 3)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityClientSellRequest_TypedDataContext1.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[1] == null)) {
                    cachedCompiledDataContext[1] = new ActivityClientSellRequest_TypedDataContext1(locations, activityContext, true);
                }
                ActivityClientSellRequest_TypedDataContext1 refDataContext3 = ((ActivityClientSellRequest_TypedDataContext1)(cachedCompiledDataContext[1]));
                return refDataContext3.GetLocation<double>(refDataContext3.ValueType___Expr3Get, refDataContext3.ValueType___Expr3Set, expressionId, this.rootActivity, activityContext);
            }
            if ((expressionId == 4)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityClientSellRequest_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext4 = ((ActivityClientSellRequest_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext4.ValueType___Expr4Get();
            }
            if ((expressionId == 5)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityClientSellRequest_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext5 = ((ActivityClientSellRequest_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext5.ValueType___Expr5Get();
            }
            if ((expressionId == 6)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityClientSellRequest_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext6 = ((ActivityClientSellRequest_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext6.ValueType___Expr6Get();
            }
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public object InvokeExpression(int expressionId, System.Collections.Generic.IList<System.Activities.Location> locations) {
            if ((this.rootActivity == null)) {
                this.rootActivity = this;
            }
            if ((expressionId == 0)) {
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext0 = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext0.ValueType___Expr0Get();
            }
            if ((expressionId == 1)) {
                ActivityClientSellRequest_TypedDataContext1 refDataContext1 = new ActivityClientSellRequest_TypedDataContext1(locations, true);
                return refDataContext1.GetLocation<string>(refDataContext1.ValueType___Expr1Get, refDataContext1.ValueType___Expr1Set);
            }
            if ((expressionId == 2)) {
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext2 = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext2.ValueType___Expr2Get();
            }
            if ((expressionId == 3)) {
                ActivityClientSellRequest_TypedDataContext1 refDataContext3 = new ActivityClientSellRequest_TypedDataContext1(locations, true);
                return refDataContext3.GetLocation<double>(refDataContext3.ValueType___Expr3Get, refDataContext3.ValueType___Expr3Set);
            }
            if ((expressionId == 4)) {
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext4 = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext4.ValueType___Expr4Get();
            }
            if ((expressionId == 5)) {
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext5 = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext5.ValueType___Expr5Get();
            }
            if ((expressionId == 6)) {
                ActivityClientSellRequest_TypedDataContext1_ForReadOnly valDataContext6 = new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext6.ValueType___Expr6Get();
            }
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public bool CanExecuteExpression(string expressionText, bool isReference, System.Collections.Generic.IList<System.Activities.LocationReference> locations, out int expressionId) {
            if (((isReference == false) 
                        && ((expressionText == "stockCurrentPriceToSell_InArg") 
                        && (ActivityClientSellRequest_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 0;
                return true;
            }
            if (((isReference == true) 
                        && ((expressionText == "stockCurrentPriceToSell_OutArg") 
                        && (ActivityClientSellRequest_TypedDataContext1.Validate(locations, true, 0) == true)))) {
                expressionId = 1;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "sellResult_InArg") 
                        && (ActivityClientSellRequest_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 2;
                return true;
            }
            if (((isReference == true) 
                        && ((expressionText == "sellResult_OutArg") 
                        && (ActivityClientSellRequest_TypedDataContext1.Validate(locations, true, 0) == true)))) {
                expressionId = 3;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "TimeSpan.FromSeconds(60)") 
                        && (ActivityClientSellRequest_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 4;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "new Exception()") 
                        && (ActivityClientSellRequest_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 5;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "new Exception()") 
                        && (ActivityClientSellRequest_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 6;
                return true;
            }
            expressionId = -1;
            return false;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public System.Collections.Generic.IList<string> GetRequiredLocations(int expressionId) {
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public System.Linq.Expressions.Expression GetExpressionTreeForExpression(int expressionId, System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) {
            if ((expressionId == 0)) {
                return new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locationReferences).@__Expr0GetTree();
            }
            if ((expressionId == 1)) {
                return new ActivityClientSellRequest_TypedDataContext1(locationReferences).@__Expr1GetTree();
            }
            if ((expressionId == 2)) {
                return new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locationReferences).@__Expr2GetTree();
            }
            if ((expressionId == 3)) {
                return new ActivityClientSellRequest_TypedDataContext1(locationReferences).@__Expr3GetTree();
            }
            if ((expressionId == 4)) {
                return new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locationReferences).@__Expr4GetTree();
            }
            if ((expressionId == 5)) {
                return new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locationReferences).@__Expr5GetTree();
            }
            if ((expressionId == 6)) {
                return new ActivityClientSellRequest_TypedDataContext1_ForReadOnly(locationReferences).@__Expr6GetTree();
            }
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityClientSellRequest_TypedDataContext0 : System.Activities.XamlIntegration.CompiledDataContext {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            public ActivityClientSellRequest_TypedDataContext0(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext0(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext0(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            internal static object GetDataContextActivitiesHelper(System.Activities.Activity compiledRoot, bool forImplementation) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetDataContextActivities(compiledRoot, forImplementation);
            }
            
            internal static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
            }
            
            public static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 0))) {
                    return false;
                }
                expectedLocationsCount = 0;
                return true;
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityClientSellRequest_TypedDataContext0_ForReadOnly : System.Activities.XamlIntegration.CompiledDataContext {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            public ActivityClientSellRequest_TypedDataContext0_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext0_ForReadOnly(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext0_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            internal static object GetDataContextActivitiesHelper(System.Activities.Activity compiledRoot, bool forImplementation) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetDataContextActivities(compiledRoot, forImplementation);
            }
            
            internal static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
            }
            
            public static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 0))) {
                    return false;
                }
                expectedLocationsCount = 0;
                return true;
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityClientSellRequest_TypedDataContext1 : ActivityClientSellRequest_TypedDataContext0 {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            protected double sellResult_OutArg;
            
            protected double sellResult_InArg;
            
            public ActivityClientSellRequest_TypedDataContext1(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext1(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext1(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            protected string stockCurrentPriceToSell_InArg {
                get {
                    return ((string)(this.GetVariableValue((0 + locationsOffset))));
                }
                set {
                    this.SetVariableValue((0 + locationsOffset), value);
                }
            }
            
            protected string stockCurrentPriceToSell_OutArg {
                get {
                    return ((string)(this.GetVariableValue((1 + locationsOffset))));
                }
                set {
                    this.SetVariableValue((1 + locationsOffset), value);
                }
            }
            
            internal new static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public new virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
                base.SetLocationsOffset(locationsOffset);
            }
            
            internal System.Linq.Expressions.Expression @__Expr1GetTree() {
                
                #line 53 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                System.Linq.Expressions.Expression<System.Func<string>> expression = () => 
              stockCurrentPriceToSell_OutArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public string @__Expr1Get() {
                
                #line 53 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                return 
              stockCurrentPriceToSell_OutArg;
                
                #line default
                #line hidden
            }
            
            public string ValueType___Expr1Get() {
                this.GetValueTypeValues();
                return this.@__Expr1Get();
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public void @__Expr1Set(string value) {
                
                #line 53 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                
              stockCurrentPriceToSell_OutArg = value;
                
                #line default
                #line hidden
            }
            
            public void ValueType___Expr1Set(string value) {
                this.GetValueTypeValues();
                this.@__Expr1Set(value);
                this.SetValueTypeValues();
            }
            
            internal System.Linq.Expressions.Expression @__Expr3GetTree() {
                
                #line 67 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                System.Linq.Expressions.Expression<System.Func<double>> expression = () => 
                  sellResult_OutArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public double @__Expr3Get() {
                
                #line 67 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                return 
                  sellResult_OutArg;
                
                #line default
                #line hidden
            }
            
            public double ValueType___Expr3Get() {
                this.GetValueTypeValues();
                return this.@__Expr3Get();
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public void @__Expr3Set(double value) {
                
                #line 67 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                
                  sellResult_OutArg = value;
                
                #line default
                #line hidden
            }
            
            public void ValueType___Expr3Set(double value) {
                this.GetValueTypeValues();
                this.@__Expr3Set(value);
                this.SetValueTypeValues();
            }
            
            protected override void GetValueTypeValues() {
                this.sellResult_OutArg = ((double)(this.GetVariableValue((2 + locationsOffset))));
                this.sellResult_InArg = ((double)(this.GetVariableValue((3 + locationsOffset))));
                base.GetValueTypeValues();
            }
            
            protected override void SetValueTypeValues() {
                this.SetVariableValue((2 + locationsOffset), this.sellResult_OutArg);
                this.SetVariableValue((3 + locationsOffset), this.sellResult_InArg);
                base.SetValueTypeValues();
            }
            
            public new static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 4))) {
                    return false;
                }
                if ((validateLocationCount == true)) {
                    offset = (locationReferences.Count - 4);
                }
                expectedLocationsCount = 4;
                if (((locationReferences[(offset + 0)].Name != "stockCurrentPriceToSell_InArg") 
                            || (locationReferences[(offset + 0)].Type != typeof(string)))) {
                    return false;
                }
                if (((locationReferences[(offset + 1)].Name != "stockCurrentPriceToSell_OutArg") 
                            || (locationReferences[(offset + 1)].Type != typeof(string)))) {
                    return false;
                }
                if (((locationReferences[(offset + 2)].Name != "sellResult_OutArg") 
                            || (locationReferences[(offset + 2)].Type != typeof(double)))) {
                    return false;
                }
                if (((locationReferences[(offset + 3)].Name != "sellResult_InArg") 
                            || (locationReferences[(offset + 3)].Type != typeof(double)))) {
                    return false;
                }
                return ActivityClientSellRequest_TypedDataContext0.Validate(locationReferences, false, offset);
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityClientSellRequest_TypedDataContext1_ForReadOnly : ActivityClientSellRequest_TypedDataContext0_ForReadOnly {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            protected double sellResult_OutArg;
            
            protected double sellResult_InArg;
            
            public ActivityClientSellRequest_TypedDataContext1_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext1_ForReadOnly(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityClientSellRequest_TypedDataContext1_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            protected string stockCurrentPriceToSell_InArg {
                get {
                    return ((string)(this.GetVariableValue((0 + locationsOffset))));
                }
            }
            
            protected string stockCurrentPriceToSell_OutArg {
                get {
                    return ((string)(this.GetVariableValue((1 + locationsOffset))));
                }
            }
            
            internal new static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public new virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
                base.SetLocationsOffset(locationsOffset);
            }
            
            internal System.Linq.Expressions.Expression @__Expr0GetTree() {
                
                #line 58 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                System.Linq.Expressions.Expression<System.Func<string>> expression = () => 
              stockCurrentPriceToSell_InArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public string @__Expr0Get() {
                
                #line 58 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                return 
              stockCurrentPriceToSell_InArg;
                
                #line default
                #line hidden
            }
            
            public string ValueType___Expr0Get() {
                this.GetValueTypeValues();
                return this.@__Expr0Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr2GetTree() {
                
                #line 72 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                System.Linq.Expressions.Expression<System.Func<double>> expression = () => 
                  sellResult_InArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public double @__Expr2Get() {
                
                #line 72 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                return 
                  sellResult_InArg;
                
                #line default
                #line hidden
            }
            
            public double ValueType___Expr2Get() {
                this.GetValueTypeValues();
                return this.@__Expr2Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr4GetTree() {
                
                #line 83 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                System.Linq.Expressions.Expression<System.Func<System.TimeSpan>> expression = () => 
                          TimeSpan.FromSeconds(60);
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public System.TimeSpan @__Expr4Get() {
                
                #line 83 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                return 
                          TimeSpan.FromSeconds(60);
                
                #line default
                #line hidden
            }
            
            public System.TimeSpan ValueType___Expr4Get() {
                this.GetValueTypeValues();
                return this.@__Expr4Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr5GetTree() {
                
                #line 90 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                System.Linq.Expressions.Expression<System.Func<System.Exception>> expression = () => 
                          new Exception();
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public System.Exception @__Expr5Get() {
                
                #line 90 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                return 
                          new Exception();
                
                #line default
                #line hidden
            }
            
            public System.Exception ValueType___Expr5Get() {
                this.GetValueTypeValues();
                return this.@__Expr5Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr6GetTree() {
                
                #line 107 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                System.Linq.Expressions.Expression<System.Func<System.Exception>> expression = () => 
                          new Exception();
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public System.Exception @__Expr6Get() {
                
                #line 107 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\ACTIVITYLIBRARY_STOCKMARKET\ACTIVITYCLIENTSELLREQUEST.XAML"
                return 
                          new Exception();
                
                #line default
                #line hidden
            }
            
            public System.Exception ValueType___Expr6Get() {
                this.GetValueTypeValues();
                return this.@__Expr6Get();
            }
            
            protected override void GetValueTypeValues() {
                this.sellResult_OutArg = ((double)(this.GetVariableValue((2 + locationsOffset))));
                this.sellResult_InArg = ((double)(this.GetVariableValue((3 + locationsOffset))));
                base.GetValueTypeValues();
            }
            
            public new static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 4))) {
                    return false;
                }
                if ((validateLocationCount == true)) {
                    offset = (locationReferences.Count - 4);
                }
                expectedLocationsCount = 4;
                if (((locationReferences[(offset + 0)].Name != "stockCurrentPriceToSell_InArg") 
                            || (locationReferences[(offset + 0)].Type != typeof(string)))) {
                    return false;
                }
                if (((locationReferences[(offset + 1)].Name != "stockCurrentPriceToSell_OutArg") 
                            || (locationReferences[(offset + 1)].Type != typeof(string)))) {
                    return false;
                }
                if (((locationReferences[(offset + 2)].Name != "sellResult_OutArg") 
                            || (locationReferences[(offset + 2)].Type != typeof(double)))) {
                    return false;
                }
                if (((locationReferences[(offset + 3)].Name != "sellResult_InArg") 
                            || (locationReferences[(offset + 3)].Type != typeof(double)))) {
                    return false;
                }
                return ActivityClientSellRequest_TypedDataContext0_ForReadOnly.Validate(locationReferences, false, offset);
            }
        }
    }
}
