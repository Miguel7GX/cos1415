//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ActivityLibrary_StockMarket {
    
    
    [System.Runtime.InteropServices.ComVisible(false)]
    public partial class ActivityClientBuyRequest : System.Activities.Activity, System.ComponentModel.ISupportInitialize {
        
        private bool _contentLoaded;
        
        private System.Activities.InArgument<string> _stockCurrentPrice_InArg;
        
        private System.Activities.OutArgument<string> _stockCurrentPrice_OutArg;
        
        private System.Activities.InArgument<string> _stockQuantity_InArg;
        
        private System.Activities.OutArgument<string> _stockQuantity_OutArg;
        
        private System.Activities.InArgument<string> _amountRequiredForPurchase_InArg;
        
        private System.Activities.OutArgument<string> _amountRequiredForPurchase_OutArg;
        
partial void BeforeInitializeComponent(ref bool isInitialized);

partial void AfterInitializeComponent();

        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("XamlBuildTask", "4.0.0.0")]
        public ActivityClientBuyRequest() {
            this.InitializeComponent();
        }
        
        public System.Activities.InArgument<string> stockCurrentPrice_InArg {
            get {
                return this._stockCurrentPrice_InArg;
            }
            set {
                this._stockCurrentPrice_InArg = value;
            }
        }
        
        public System.Activities.OutArgument<string> stockCurrentPrice_OutArg {
            get {
                return this._stockCurrentPrice_OutArg;
            }
            set {
                this._stockCurrentPrice_OutArg = value;
            }
        }
        
        public System.Activities.InArgument<string> stockQuantity_InArg {
            get {
                return this._stockQuantity_InArg;
            }
            set {
                this._stockQuantity_InArg = value;
            }
        }
        
        public System.Activities.OutArgument<string> stockQuantity_OutArg {
            get {
                return this._stockQuantity_OutArg;
            }
            set {
                this._stockQuantity_OutArg = value;
            }
        }
        
        public System.Activities.InArgument<string> amountRequiredForPurchase_InArg {
            get {
                return this._amountRequiredForPurchase_InArg;
            }
            set {
                this._amountRequiredForPurchase_InArg = value;
            }
        }
        
        public System.Activities.OutArgument<string> amountRequiredForPurchase_OutArg {
            get {
                return this._amountRequiredForPurchase_OutArg;
            }
            set {
                this._amountRequiredForPurchase_OutArg = value;
            }
        }
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("XamlBuildTask", "4.0.0.0")]
        public void InitializeComponent() {
            if ((this._contentLoaded == true)) {
                return;
            }
            this._contentLoaded = true;
            bool isInitialized = false;
            this.BeforeInitializeComponent(ref isInitialized);
            if ((isInitialized == true)) {
                this.AfterInitializeComponent();
                return;
            }
            string resourceName = this.FindResource();
            System.IO.Stream initializeXaml = typeof(ActivityClientBuyRequest).Assembly.GetManifestResourceStream(resourceName);
            System.Xml.XmlReader xmlReader = null;
            System.Xaml.XamlReader reader = null;
            System.Xaml.XamlObjectWriter objectWriter = null;
            try {
                System.Xaml.XamlSchemaContext schemaContext = XamlStaticHelperNamespace._XamlStaticHelper.SchemaContext;
                xmlReader = System.Xml.XmlReader.Create(initializeXaml);
                System.Xaml.XamlXmlReaderSettings readerSettings = new System.Xaml.XamlXmlReaderSettings();
                readerSettings.LocalAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                readerSettings.AllowProtectedMembersOnRoot = true;
                reader = new System.Xaml.XamlXmlReader(xmlReader, schemaContext, readerSettings);
                System.Xaml.XamlObjectWriterSettings writerSettings = new System.Xaml.XamlObjectWriterSettings();
                writerSettings.RootObjectInstance = this;
                writerSettings.AccessLevel = System.Xaml.Permissions.XamlAccessLevel.PrivateAccessTo(typeof(ActivityClientBuyRequest));
                objectWriter = new System.Xaml.XamlObjectWriter(schemaContext, writerSettings);
                System.Xaml.XamlServices.Transform(reader, objectWriter);
            }
            finally {
                if ((xmlReader != null)) {
                    ((System.IDisposable)(xmlReader)).Dispose();
                }
                if ((reader != null)) {
                    ((System.IDisposable)(reader)).Dispose();
                }
                if ((objectWriter != null)) {
                    ((System.IDisposable)(objectWriter)).Dispose();
                }
            }
            this.AfterInitializeComponent();
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("XamlBuildTask", "4.0.0.0")]
        private string FindResource() {
            string[] resources = typeof(ActivityClientBuyRequest).Assembly.GetManifestResourceNames();
            for (int i = 0; (i < resources.Length); i = (i + 1)) {
                string resource = resources[i];
                if ((resource.Contains(".ActivityClientBuyRequest.g.xaml") || resource.Equals("ActivityClientBuyRequest.g.xaml"))) {
                    return resource;
                }
            }
            throw new System.InvalidOperationException("Resource not found.");
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033")]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("XamlBuildTask", "4.0.0.0")]
        void System.ComponentModel.ISupportInitialize.BeginInit() {
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033")]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("XamlBuildTask", "4.0.0.0")]
        void System.ComponentModel.ISupportInitialize.EndInit() {
            this.InitializeComponent();
        }
    }
}
