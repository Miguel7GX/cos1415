﻿using ActivityLibrary_StockMarket;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebRoleMyBroker.BrokerAdmin
{
    public partial class AuthorizeTransactions : System.Web.UI.Page
    {

        // Variáveis 
        public static WorkflowApplication wf = null;

        // IDictionary (onde ficam armazenados os argumentos)
        IDictionary<string, object> inputBuyBroker = new Dictionary<string, object>();
        IDictionary<string, object> inputSellBroker = new Dictionary<string, object>();
     

        protected void Page_Load(object sender, EventArgs e)
        {

            if (User.Identity.Name != "admin@mail.pt")
            {
                Response.Redirect("~/Default.aspx");
            }         
        }


        // Regista Operação de Compra na tabela ClientWallet
        public static void RegisterOperationClientWallet(string OperationID, string userID, string clientBankAccount, string stockQuote, int stockAmount, double operationCost)
        {
            Utils.SQLUtils.SQLRegisterOperationClientWallet("INSERT INTO ClientWallet (OperationID,ClientId,ClientBankAccount,Stock,StockAmount,TransactionValue) VALUES (@OperationID,@userID,@clientBankAccount,@stockQuote,@stockAmount,@operationCost)", OperationID, userID, clientBankAccount, stockQuote, stockAmount, operationCost);
        }


        protected void GridView_ClientOperations_SelectedIndexChanged(object sender, EventArgs e)
        {

            System.Net.ServicePointManager.Expect100Continue = false;

            // ******************* Serviços Bolsa de Valores ******************* 
            ServiceReferenceGetQuotes.StockQuoteServiceClient stockService = new ServiceReferenceGetQuotes.StockQuoteServiceClient();
            // ******************* Serviços Bolsa de Valores *******************

            // ******************* Serviço Bancário ****************************
            ServiceReferenceBankAccount.BankServiceSoapClient bankService = new ServiceReferenceBankAccount.BankServiceSoapClient();
            // ******************* Serviço Bancário ****************************

            // Determina o index da linha clicada
            int rowIndex = GridView_ClientOperations.SelectedRow.RowIndex;

            // Para determinar o ID da operação e recolher os dados para registo na BD da My Broker (Peocesso de compra) 
            GridViewRow row = GridView_ClientOperations.SelectedRow;

            int accountNr = int.Parse(row.Cells[3].Text);
            double amountForBuy = double.Parse(row.Cells[6].Text);

            // Variável que fica com o valor true ou false, consoante há ou não autorização do banco para a transação.
            bool isPurchaseAuthorizedByTheBank = bankService.AuthorizeOperation(accountNr, amountForBuy);
            bool isSellAuthorizedByTheBank = bankService.doesBankAccountExist(accountNr);

             if (isPurchaseAuthorizedByTheBank.Equals(false))
                {

                    // Autorização do Banco não concedida
                }

                else if (isPurchaseAuthorizedByTheBank.Equals(true))
                {

                    // Valor total da operação 
                    double operationAmount = double.Parse(row.Cells[6].Text);


                    // IDictionary (Adicionar os argumentos)
                    inputBuyBroker.Clear();

                      // IDictionary (Adicionar os argumentos)
                    inputBuyBroker.Add("totalAmountRequiredForPurchase_InArg", operationAmount);
                    inputBuyBroker.Add("BankConfirmation_InArg", isPurchaseAuthorizedByTheBank);                
                    inputBuyBroker.Add("BrokerConfirmation_InArg", true);

                    // IDictionary (Adicionar os argumentos)                
                    inputSellBroker.Add("totalAmountOfSale_InArg", operationAmount);
                    inputSellBroker.Add("BankConfirmationToSell_InArg", isSellAuthorizedByTheBank);  
                    inputSellBroker.Add("BrokerConfirmationToSell_InArg", true);

                   

                    ActivityBuyStock BuyWorkflow = new ActivityLibrary_StockMarket.ActivityBuyStock();
                    ActivitySellStock SellWorkflow = new ActivitySellStock();

                    wf = new WorkflowApplication(BuyWorkflow, inputBuyBroker);
                    wf = new WorkflowApplication(SellWorkflow, inputSellBroker);

                    wf.Completed = delegate(WorkflowApplicationCompletedEventArgs x)
                    {
                        if (x.CompletionState == ActivityInstanceState.Faulted)
                        {
                            Console.WriteLine("Workflow {0} Terminated.", x.InstanceId);
                        }
                        else if (x.CompletionState == ActivityInstanceState.Canceled)
                        {
                            Console.WriteLine("Workflow {0} Canceled.", x.InstanceId);
                        }
                        else
                        {
                            Console.WriteLine("Workflow {0} Completed.", x.InstanceId);
                        }


                        if ((row.Cells[7].Text.Trim()) == "Buy")
                        {

                            // Verifica qual o proximo nr de Operation ID disponível (Para inserção da operação na BD da MyBroker - tabela ClientsWallet)
                            string operationNumber = Convert.ToString((Utils.SQLUtils.SQLCheckNextOperationNumber("SELECT COUNT(*) FROM ClientWallet") + 1));
                            string userID = row.Cells[2].Text.Trim();
                            string bankAccount = row.Cells[3].Text.Trim();
                            string stock = row.Cells[4].Text.Trim();
                            int amount = int.Parse(row.Cells[5].Text.Trim());
                            double tranValue = double.Parse(row.Cells[6].Text.Trim());

                            // Retira dinheiro da conta bancária do Cliente.
                            bankService.Withdraw(int.Parse(bankAccount), tranValue);

                            // Regista a operação de compra na base de dados da MyBroker - tabela ClientWallet
                            RegisterOperationClientWallet(operationNumber, userID, bankAccount, stock, amount, tranValue);


                            // *********************** Remove linha selecionada para aprovação na MyBroker *********************** 
                            // Apaga a linha
                            GridView_ClientOperations.DeleteRow(rowIndex);

                            // Efetua sincronismo com a BD
                            GridView_ClientOperations.DataBind();

                        }


                        else if ((row.Cells[7].Text.Trim()) == "Sell")
                        {

                            // Efetua depósito na conta bancária
                            bankService.Deposit(int.Parse(row.Cells[3].Text.Trim()), double.Parse(row.Cells[6].Text.Trim()));
           
                            string opID = Utils.SQLUtils.SQLselectCommandClientWallet("SELECT OperationID FROM ClientWallet WHERE (ClientId=@userID AND Stock=@stock)", row.Cells[2].Text.Trim(), row.Cells[4].Text.Trim());
                            
                            // Remove a operação da tabela de aprovações do admin da MyBroker
                            Utils.SQLUtils.DeleteOperation("DELETE FROM ClientWallet WHERE OperationID=@OperationID", opID);

                            // *********************** Remove linha selecionada para aprovação na MyBroker *********************** 
                            // Apaga a linha da tabela de aprovações do admin da MyBroker
                            GridView_ClientOperations.DeleteRow(rowIndex);

                            // Efetua sincronismo com a BD
                            GridView_ClientOperations.DataBind();

                        }
                    };

                    wf.Run();
     
                }

        }
    }
}