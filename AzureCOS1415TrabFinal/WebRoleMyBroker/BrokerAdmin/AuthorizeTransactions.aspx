﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuthorizeTransactions.aspx.cs" Inherits="WebRoleMyBroker.BrokerAdmin.AuthorizeTransactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <asp:GridView ID="GridView_ClientOperations" runat="server" Width="393px" AutoGenerateColumns="False" DataKeyNames="OperationID" DataSourceID="SqlDataSource_ClientOperations" OnSelectedIndexChanged="GridView_ClientOperations_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="OperationID" HeaderText="OperationID" ReadOnly="True" SortExpression="OperationID" />
            <asp:BoundField DataField="ClientId" HeaderText="ClientId" SortExpression="ClientId" />
            <asp:BoundField DataField="ClientBankAccount" HeaderText="ClientBankAccount" SortExpression="ClientBankAccount" />
            <asp:BoundField DataField="Stock" HeaderText="Stock" SortExpression="Stock" />
            <asp:BoundField DataField="StockAmount" HeaderText="StockAmount" SortExpression="StockAmount" />
            <asp:BoundField DataField="TransactionValue" HeaderText="TransactionValue" SortExpression="TransactionValue" />
            <asp:BoundField DataField="OperationType" HeaderText="OperationType" SortExpression="OperationType" />
        </Columns>
    </asp:GridView>
<asp:SqlDataSource ID="SqlDataSource_ClientOperations" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString_ClientOperations %>" SelectCommand="SELECT * FROM [ClientOperations]" DeleteCommand="DELETE FROM [ClientOperations] WHERE [OperationID] = @OperationID" InsertCommand="INSERT INTO [ClientOperations] ([OperationID], [ClientId], [ClientBankAccount], [Stock], [StockAmount], [TransactionValue], [OperationType], [AuthorizedByBroker]) VALUES (@OperationID, @ClientId, @ClientBankAccount, @Stock, @StockAmount, @TransactionValue, @OperationType, @AuthorizedByBroker)" UpdateCommand="UPDATE [ClientOperations] SET [ClientId] = @ClientId, [ClientBankAccount] = @ClientBankAccount, [Stock] = @Stock, [StockAmount] = @StockAmount, [TransactionValue] = @TransactionValue, [OperationType] = @OperationType, [AuthorizedByBroker] = @AuthorizedByBroker WHERE [OperationID] = @OperationID">
    <DeleteParameters>
        <asp:Parameter Name="OperationID" Type="String" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="OperationID" Type="String" />
        <asp:Parameter Name="ClientId" Type="String" />
        <asp:Parameter Name="ClientBankAccount" Type="String" />
        <asp:Parameter Name="Stock" Type="String" />
        <asp:Parameter Name="StockAmount" Type="Int32" />
        <asp:Parameter Name="TransactionValue" Type="Double" />
        <asp:Parameter Name="OperationType" Type="String" />
        <asp:Parameter Name="AuthorizedByBroker" Type="Boolean" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="ClientId" Type="String" />
        <asp:Parameter Name="ClientBankAccount" Type="String" />
        <asp:Parameter Name="Stock" Type="String" />
        <asp:Parameter Name="StockAmount" Type="Int32" />
        <asp:Parameter Name="TransactionValue" Type="Double" />
        <asp:Parameter Name="OperationType" Type="String" />
        <asp:Parameter Name="AuthorizedByBroker" Type="Boolean" />
        <asp:Parameter Name="OperationID" Type="String" />
    </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    </asp:Content>
