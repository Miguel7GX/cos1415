﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebRoleMyBroker.Startup))]
namespace WebRoleMyBroker
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
