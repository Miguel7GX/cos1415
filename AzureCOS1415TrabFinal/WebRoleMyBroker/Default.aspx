﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebRoleMyBroker._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <table style="width: 100%;">

            <tr>
                <td style="width: 242px">
                     <asp:Image ID="ImageLogo" runat="server" Height="113px" ImageUrl="~/Pics/Pic_Default.jpeg" Width="222px"/>
                </td>
                <td>
                    <h1>My Broker Trading Inc</h1>   
                    <p class="lead">A Wall Street trading company operating in the global market since 2014.</p>
                </td>
            </tr>
        </table>

    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Começe aqui...</h2>
            <br />
            <p>
                <a href="/Operations/GetQuoteInfo.aspx" class="btn btn-primary btn-lg">Procurar Títulos &raquo;</a>
            </p>
            <br />
            <br />
            <br />
        </div>
        <div class="col-md-4">
            <h2>As minhas operações</h2>
            <br />
            <p>
               &nbsp;<a class="btn btn-default" href="/Operations/CheckPortfolio.aspx">Consultar Portfólio&nbsp;&nbsp;&nbsp;&nbsp; &raquo;</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
            <p>
                &nbsp;<a class="btn btn-default" href="/Operations/CheckOperations.aspx">Consultar Operações &raquo;</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
            <p>
                &nbsp;<a class="btn btn-default" href="/Operations/PlaceBuyOrder.aspx">Comprar Título&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &raquo;</a>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
            <p>
                &nbsp;<a class="btn btn-default" href="/Operations/PlaceSellOrder.aspx">Vender Título&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &raquo;</a>
           </p>
             <p>
                &nbsp;<a class="btn btn-default" href="/BrokerAdmin/AuthorizeTransactions.aspx">Autorizar Operações&nbsp;&nbsp; &raquo;</a>
           </p>
        </div>
        <div class="col-md-4">
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>

</asp:Content>
