﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebRoleMyBroker.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>My Broker Trading Inc</h3>
    <p>We buy &amp; sell stock and operate in the Wall Street market since 2014.</p>
    <p><br /><asp:Image ID="ImageAbout" runat="server" ImageUrl="~/Pics/Pic_About.jpg" Height="282px" Width="252px"/></p>
    
</asp:Content>
