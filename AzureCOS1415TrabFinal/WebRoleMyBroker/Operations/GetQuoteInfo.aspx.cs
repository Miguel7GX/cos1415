﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebRoleMyBroker.Operations
{
    public partial class GetQuoteInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonGetQuote_Click(object sender, EventArgs e)
        {

            StringBuilder imgUrl;

            System.Net.ServicePointManager.Expect100Continue = false;
       
            ServiceReferenceGetQuotes.StockQuoteServiceClient quoteService = new ServiceReferenceGetQuotes.StockQuoteServiceClient();

            try
            {
                LabelName.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).Name;
                LabelQuoteSymbol.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).Symbol;
                LabelMarketCap.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).MktCap;
                LabelQuoteCurrentValue.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).Last;
                LabelQuoteSymbol.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).Symbol;
                LabelQuoteHigh.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).High;
                LabelQuoteLow.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).Low;
                LabelPercent.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).PercentageChange;
                LabelQuoteHigh.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).High;
                LabelQuoteVolume.Text = quoteService.GetStockQuote(TextBoxGetQuote.Text).Volume + " títulos";

                imgUrl = new System.Text.StringBuilder();
                imgUrl.Append("http://bigcharts.marketwatch.com/kaavio.Webhost/charts/big.chart?nosettings=1&amp;symb=");
                imgUrl.Append(TextBoxGetQuote.Text);
                imgUrl.Append("&amp;uf=0&amp;type=2&amp;size=2&amp;sid=41843&amp;style=320&amp;freq=1&amp;entitlementtoken=0c33378313484ba9b46b8e24ded87dd6&amp;time=8&amp;rand=501442721&amp;compidx=&amp;ma=0&amp;maval=9&amp;lf=1&amp;lf2=0&amp;lf3=0&amp;height=335&amp;width=579&amp;mocktick=1");

                QuotechartIFrame.Visible = true;
                QuotechartIFrame.Attributes.Add("src", imgUrl.ToString());
            }
            
            catch (Exception ex)
            {
                ex.Message.ToString();
            }

            finally
            {
                quoteService.Close();
            }
        }
     
    }
}