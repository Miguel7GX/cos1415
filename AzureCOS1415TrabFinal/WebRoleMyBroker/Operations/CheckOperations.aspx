﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckOperations.aspx.cs" Inherits="WebRoleMyBroker.Operations.CheckOperations" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
    </p>
    <p>
    </p>
    <p>
     <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource_CheckClientOperations">
            <Columns>
                <asp:BoundField DataField="ClientBankAccount" HeaderText="ClientBankAccount" SortExpression="ClientBankAccount" />
                <asp:BoundField DataField="Stock" HeaderText="Stock" SortExpression="Stock" />
                <asp:BoundField DataField="StockAmount" HeaderText="StockAmount" SortExpression="StockAmount" />
                <asp:BoundField DataField="TransactionValue" HeaderText="TransactionValue" SortExpression="TransactionValue" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource_CheckClientOperations" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString_ClientOperations %>" SelectCommand="SELECT [ClientBankAccount], [Stock], [StockAmount], [TransactionValue] FROM [ClientOperations] WHERE ([ClientId] = @ClientId)">
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="ClientId" Type="String" />
            </SelectParameters>
            
        </asp:SqlDataSource>
    </p>
</asp:Content>
