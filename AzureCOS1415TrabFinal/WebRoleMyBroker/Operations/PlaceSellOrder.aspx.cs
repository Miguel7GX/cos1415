﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebRoleMyBroker.Operations
{
    public partial class PlaceSellOrder : System.Web.UI.Page
    {

        // IDictionary (onde ficam armazenados os argumentos)
        IDictionary<string, object> inputClient = new Dictionary<string, object>();

        public static bool isElapsedTimeToSellOver1min = false;

        // obtem a cotação actual do título 
        public static string currentStockValue = null;

        // Valor resultante do processo de venda dos títulos
        public static double saleResult = 0.0;

        // Título
        public static string stock = "";

        // Montante de Títulos a Comprar 
        public static int stockAmount = 0;

        // Número da conta Bancária do Cliente 
        public static string accountNr = null;

        public static WorkflowApplication wfClientSellConfirmation = null;

        // ******************* Serviços a utilizar ******************* 
        // Serviço que obtem os dados da Bolsa de Valores
        static ServiceReferenceGetQuotes.StockQuoteServiceClient stockService = new ServiceReferenceGetQuotes.StockQuoteServiceClient();

        // Serviço que obtem o Saldo bancário da conta do Cliente
        static ServiceReferenceBankAccount.BankServiceSoapClient bankService = new ServiceReferenceBankAccount.BankServiceSoapClient();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated.Equals(false))
            {
                Response.Redirect("~/Default.aspx");
            }
            else if (User.Identity.IsAuthenticated.Equals(true))
            {
                string ID = User.Identity.Name.ToString();
                SqlDataSource_CheckClientOperations.SelectParameters[0].DefaultValue = ID;
            }
        }


        public void TimeIsOut()
        {
            isElapsedTimeToSellOver1min = true;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            System.Net.ServicePointManager.Expect100Continue = false;

            // Para determinar o ID da operação e recolher os dados para registo na BD da My Broker (Peocesso de compra) 
            GridViewRow row = GridView1.SelectedRow;
            stock = row.Cells[3].Text.Trim(); //Coluna Stock (tabela ClientWallet)
            stockAmount = Convert.ToInt32(row.Cells[4].Text.Trim()); //Coluna Quantidade de títulos (tabela ClientWallet)
        
            accountNr = row.Cells[2].Text.Trim(); //Coluna conta bancária (tabela ClientWallet)


            string currentStockValue = stockService.GetStockQuote(row.Cells[3].Text.Trim()).Last;

            // Montante para o processo de venda dos títulos
            saleResult = ((Convert.ToDouble(currentStockValue)) * stockAmount / 100);

            Label_Stock .Text= row.Cells[3].Text.Trim();
            Label_stockValue.Text = currentStockValue;

             //IDictionary (Adicionar os argumentos)
            inputClient.Add("stockCurrentPriceToSell_InArg", currentStockValue);
            inputClient.Add("sellResult_InArg", saleResult);

            
            // Instanciação da Activity Library 
            ActivityLibrary_StockMarket.ActivityClientSellRequest ClientRequest = new ActivityLibrary_StockMarket.ActivityClientSellRequest();

             wfClientSellConfirmation = new WorkflowApplication(ClientRequest, inputClient);
            
                wfClientSellConfirmation.Completed = delegate(WorkflowApplicationCompletedEventArgs x)
                {
                    if (x.CompletionState == ActivityInstanceState.Faulted)
                    {
                        Console.WriteLine("Workflow {0} Terminated.", x.InstanceId);
                  
                    }
                    else if (x.CompletionState == ActivityInstanceState.Canceled)
                    {
                        Console.WriteLine("Workflow {0} Canceled.", x.InstanceId);
                    }
                    else
                    {
                        Console.WriteLine("Workflow {0} Completed.", x.InstanceId);          
                    }

                };

                wfClientSellConfirmation.Run();

                Label_stockValue .Text = currentStockValue.ToString();
                Label_SaleResult.Text = saleResult.ToString();
           }

      
        // Método para inserir operação do cliente na base de dados da MyBroker
        public static void RegisterOperation(string OperationID, string userID, string clientBankAccount, string stockQuote, int stockAmount, double operationCost, string opType)
        {
            // Regista pedido de operação na base de dados
            Utils.SQLUtils.SQLRegisterOperation("INSERT INTO ClientOperations (OperationID,ClientId,ClientBankAccount,Stock,StockAmount,TransactionValue,OperationType) VALUES (@OperationID,@userID,@clientBankAccount,@stockQuote,@stockAmount,@operationCost,@opType)", OperationID, userID, clientBankAccount, stockQuote, stockAmount, operationCost, opType);
        }

        protected void Button_ConfirmStockSell_Click(object sender, EventArgs e)
        {

             if(isElapsedTimeToSellOver1min.Equals(true))
            {
                Response.Redirect("~/Default.aspx");
            }

             else if (isElapsedTimeToSellOver1min.Equals(false))
             {

                 // Resume Bookmark
                 wfClientSellConfirmation.ResumeBookmark("ClientConfirmsSellOperation", string.Empty);

                 // Verifica qual o proximo nr de Operation ID disponível
                 string clientRequestOperationNumber = Convert.ToString((Utils.SQLUtils.SQLCheckNextOperationNumber("SELECT COUNT(*) FROM ClientOperations") + 1));

                 // Regista venda na base de dados da MyBroker
                 RegisterOperation(clientRequestOperationNumber, User.Identity.Name, accountNr, stock, stockAmount, saleResult, "Sell");

                 // Limpa IDictionary
                 inputClient.Clear();

                 Response.Redirect("/Default.aspx");
             }
        }
    }
}