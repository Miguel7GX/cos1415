﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Activities;
using ActivityLibrary_StockMarket;
using System.Activities.Statements;
using System.Web.Providers.Entities;


namespace WebRoleMyBroker.Operations
{



    public partial class PlaceBuyOrder : System.Web.UI.Page
    {

        // IDictionary (onde ficam armazenados os argumentos)
        IDictionary<string, object> inputClient = new Dictionary<string, object>();


        public static bool isElapsedTimeToBuyOver1min = false;

            // obtem a cotação actual do título 
            public static string currentStockValue = null;

            // Valor necessário para o processo de compra dos títulos
            public static double amountRequiredForPurchase = 0.0;

            // Montante de Títulos a Comprar 
            public static int stockAmountToBuy = 0;
         
           // Número da conta Bancária do Cliente 
            public static string accountNr = null;

            
            public static WorkflowApplication wfClientConfirmation = null;

            // ******************* Serviços a utilizar ******************* 
            // Serviço que obtem os dados da Bolsa de Valores
            static ServiceReferenceGetQuotes.StockQuoteServiceClient stockService = new ServiceReferenceGetQuotes.StockQuoteServiceClient();

            // Serviço que obtem o Saldo bancário da conta do Cliente
            static ServiceReferenceBankAccount.BankServiceSoapClient bankService = new ServiceReferenceBankAccount.BankServiceSoapClient();


        protected void Page_Load(object sender, EventArgs e)
        {        

            if(User.Identity.IsAuthenticated.Equals(false))
            {
                Response.Redirect("~/Default.aspx");
            }
            
        }


    public void TimeIsOut()
    {
    
        isElapsedTimeToBuyOver1min = true;
    }

        protected void Button_SearchStock_Click(object sender, EventArgs e)
        {

            System.Net.ServicePointManager.Expect100Continue = false;

           
            
            // obtem a cotação actual do título 
            currentStockValue = stockService.GetStockQuote(TextBox_StockToBuy.Text.Trim()).Last;
            stockAmountToBuy = int.Parse(TextBox_StockAmount.Text.Trim());

            if(currentStockValue != null)
            {

                double stockToBuy = double.Parse(stockService.GetStockQuote(TextBox_StockToBuy.Text.Trim()).Last);
                int stockAmount = int.Parse(TextBox_StockAmount.Text.Trim());

                amountRequiredForPurchase = ((stockToBuy * stockAmount) / 100);
                accountNr = TextBox_ClientBankAccount.Text.Trim();

           
                    // IDictionary (Adicionar os argumentos)             
                    inputClient.Add("stockCurrentPrice_InArg", Convert.ToString(stockToBuy));                
                    inputClient.Add("stockQuantity_InArg", Convert.ToString(stockAmount));
                    inputClient.Add("amountRequiredForPurchase_InArg", Convert.ToString(amountRequiredForPurchase));
                
                    // Instanciação da Activity Library         
                    WebRoleMyBroker.WF.ActivityClientBuyRequest ClientRequest = new WF.ActivityClientBuyRequest();
           
                wfClientConfirmation = new WorkflowApplication(ClientRequest, inputClient);
          

                wfClientConfirmation.Completed = delegate(WorkflowApplicationCompletedEventArgs x)
                {
                    if (x.CompletionState == ActivityInstanceState.Faulted)
                    {
                        Console.WriteLine("Workflow {0} Terminated.", x.InstanceId);
                   
                    }
                    else if (x.CompletionState == ActivityInstanceState.Canceled)
                    {
                        Console.WriteLine("Workflow {0} Canceled.", x.InstanceId);
                    }
                    else
                    {
                        Console.WriteLine("Workflow {0} Completed.", x.InstanceId);                  
                    }
                    
                };

                wfClientConfirmation.Run();

                Label_StockValue.Text = currentStockValue;
                Label_RequiredAmount.Text = Convert.ToString(amountRequiredForPurchase);
                }


       
                else if(currentStockValue == null)
                {
                    //"Título introduzido não existe";
                    Response.Redirect("~/Default.aspx");
                }


        }

        // Método para inserir operação do cliente na base de dados da MyBroker
            public static void RegisterOperation(string OperationID, string userID, string clientBankAccount, string stockQuote, int stockAmount, double operationCost, string opType)
        {
            // Regista pedido de operação na base de dados
            Utils.SQLUtils.SQLRegisterOperation("INSERT INTO ClientOperations (OperationID,ClientId,ClientBankAccount,Stock,StockAmount,TransactionValue,OperationType) VALUES (@OperationID,@userID,@clientBankAccount,@stockQuote,@stockAmount,@operationCost,@opType)", OperationID, userID, clientBankAccount, stockQuote, stockAmount, operationCost, opType);
        }


        // Cliente confirma Operação
        protected void Button_ConfirmOperation_Click(object sender, EventArgs e)
        {

             if(isElapsedTimeToBuyOver1min.Equals(true))
            {
                Response.Redirect("~/Default.aspx");
            }

             else if (isElapsedTimeToBuyOver1min.Equals(false))
             {


                 // Valor do título a comprar
                 double stockToBuy = (double.Parse(stockService.GetStockQuote(TextBox_StockToBuy.Text.Trim()).Last) / 100);

                 //Quantidade 
                 int stockAmount = int.Parse(TextBox_StockAmount.Text.Trim());

                 // Montante para o processo de compra dos títulos
                 amountRequiredForPurchase = (stockToBuy * stockAmount);


                 // Resume Bookmark
                 wfClientConfirmation.ResumeBookmark("ClientConfirmsOperation", string.Empty);

                 // Verifica qual o proximo nr de Operation ID disponível
                 string clientRequestOperationNumber = Convert.ToString((Utils.SQLUtils.SQLCheckNextOperationNumber("SELECT COUNT(*) FROM ClientOperations") + 1));


                 string user = User.Identity.Name;
                 string bankAccount = TextBox_ClientBankAccount.Text.Trim();
                 string stockQuote = TextBox_StockToBuy.Text.Trim();

                 RegisterOperation(clientRequestOperationNumber, user, bankAccount, stockQuote, stockAmount, amountRequiredForPurchase, "Buy");

                 // Limpa IDictionary
                 inputClient.Clear();

                 Response.Redirect("/Default.aspx");
             }
        }
    }
}