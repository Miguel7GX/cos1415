﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebRoleMyBroker.Operations
{
    public partial class CheckOperations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated.Equals(false))
            {
                Response.Redirect("~/Default.aspx");
            }
            else if (User.Identity.IsAuthenticated.Equals(true))
            {

                string ID = User.Identity.Name.ToString();
                SqlDataSource_CheckClientOperations.SelectParameters[0].DefaultValue = ID;
            }
        }
    }
}