﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaceSellOrder.aspx.cs" Inherits="WebRoleMyBroker.Operations.PlaceSellOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource_CheckClientOperations" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource_CheckClientOperations" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString_ClientOperations %>" SelectCommand="SELECT [OperationId], [ClientBankAccount], [Stock], [StockAmount], [TransactionValue] FROM [ClientWallet] WHERE ([ClientId] = @ClientId)">
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="ClientId" Type="String" />
            </SelectParameters>
            
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;</p>
    <p>
        Título a vender:&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label_Stock" runat="server" Text=""></asp:Label>
        <br />

    </p>
    <p>
        Valor do título:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label_stockValue" runat="server" Text=""></asp:Label>

    </p>
    Resultado venda: <asp:Label ID="Label_SaleResult" runat="server" Text=""></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button_ConfirmStockSell" runat="server" Text="Confirmar Venda" OnClick="Button_ConfirmStockSell_Click" />
</asp:Content>
