﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GetQuoteInfo.aspx.cs" Inherits="WebRoleMyBroker.Operations.GetQuoteInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

  <br />
    <br />
    Symbolo: <asp:TextBox ID="TextBoxGetQuote" runat="server"></asp:TextBox>
    <asp:Button ID="ButtonGetQuote" runat="server" Text="Pesquisar" OnClick="ButtonGetQuote_Click" />
    <br /><br />
    
    Nome: <asp:Label ID="LabelName" runat="server" Text=""></asp:Label><br />
    Simbolo: <asp:Label ID="LabelQuoteSymbol" runat="server" Text=""></asp:Label><br />
    Valor de mercado da Empresa: <asp:Label ID="LabelMarketCap" runat="server" Text=""></asp:Label>
    <br /><br />
    Cotação atual: <asp:Label ID="LabelQuoteCurrentValue" runat="server" Text=""></asp:Label><br />
    Valor mais alto: <asp:Label ID="LabelQuoteHigh" runat="server" Text=""></asp:Label><br />
    Valor mais baixo: <asp:Label ID="LabelQuoteLow" runat="server" Text=""></asp:Label><br />
    Alteração Percentual: <asp:Label ID="LabelPercent" runat="server" Text=""></asp:Label><br />
    Volume: <asp:Label ID="LabelQuoteVolume" runat="server" Text=""></asp:Label>  
    <br />
    <br />
    <br />
    <asp:Image ID="ImageChart" runat="server" />
    <iframe id ="QuotechartIFrame" runat="server" visible ="false" style="height: 366px; width: 591px"></iframe>

</asp:Content>
