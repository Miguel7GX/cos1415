namespace WebRoleMyBroker.WF {
    
    #line 25 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Collections;
    
    #line default
    #line hidden
    
    #line 26 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Collections.Generic;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Activities;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Activities.Expressions;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Activities.Statements;
    
    #line default
    #line hidden
    
    #line 27 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Data;
    
    #line default
    #line hidden
    
    #line 28 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Linq;
    
    #line default
    #line hidden
    
    #line 29 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Text;
    
    #line default
    #line hidden
    
    #line 1 "D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\WF\ActivityBuyStock.xaml"
    using System.Activities.XamlIntegration;
    
    #line default
    #line hidden
    
    
    public partial class ActivityBuyStock : System.Activities.XamlIntegration.ICompiledExpressionRoot {
        
        private System.Activities.Activity rootActivity;
        
        private object dataContextActivities;
        
        private bool forImplementation = true;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public string GetLanguage() {
            return "C#";
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public object InvokeExpression(int expressionId, System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext) {
            if ((this.rootActivity == null)) {
                this.rootActivity = this;
            }
            if ((this.dataContextActivities == null)) {
                this.dataContextActivities = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetDataContextActivitiesHelper(this.rootActivity, this.forImplementation);
            }
            if ((expressionId == 0)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext0 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext0.ValueType___Expr0Get();
            }
            if ((expressionId == 1)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[1] == null)) {
                    cachedCompiledDataContext[1] = new ActivityBuyStock_TypedDataContext1(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1 refDataContext1 = ((ActivityBuyStock_TypedDataContext1)(cachedCompiledDataContext[1]));
                return refDataContext1.GetLocation<double>(refDataContext1.ValueType___Expr1Get, refDataContext1.ValueType___Expr1Set, expressionId, this.rootActivity, activityContext);
            }
            if ((expressionId == 2)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext2 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext2.ValueType___Expr2Get();
            }
            if ((expressionId == 3)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[1] == null)) {
                    cachedCompiledDataContext[1] = new ActivityBuyStock_TypedDataContext1(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1 refDataContext3 = ((ActivityBuyStock_TypedDataContext1)(cachedCompiledDataContext[1]));
                return refDataContext3.GetLocation<bool>(refDataContext3.ValueType___Expr3Get, refDataContext3.ValueType___Expr3Set, expressionId, this.rootActivity, activityContext);
            }
            if ((expressionId == 4)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext4 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext4.ValueType___Expr4Get();
            }
            if ((expressionId == 5)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[1] == null)) {
                    cachedCompiledDataContext[1] = new ActivityBuyStock_TypedDataContext1(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1 refDataContext5 = ((ActivityBuyStock_TypedDataContext1)(cachedCompiledDataContext[1]));
                return refDataContext5.GetLocation<bool>(refDataContext5.ValueType___Expr5Get, refDataContext5.ValueType___Expr5Set, expressionId, this.rootActivity, activityContext);
            }
            if ((expressionId == 6)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext6 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext6.ValueType___Expr6Get();
            }
            if ((expressionId == 7)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext7 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext7.ValueType___Expr7Get();
            }
            if ((expressionId == 8)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext8 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext8.ValueType___Expr8Get();
            }
            if ((expressionId == 9)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext9 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext9.ValueType___Expr9Get();
            }
            if ((expressionId == 10)) {
                System.Activities.XamlIntegration.CompiledDataContext[] cachedCompiledDataContext = ActivityBuyStock_TypedDataContext1_ForReadOnly.GetCompiledDataContextCacheHelper(this.dataContextActivities, activityContext, this.rootActivity, this.forImplementation, 2);
                if ((cachedCompiledDataContext[0] == null)) {
                    cachedCompiledDataContext[0] = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, activityContext, true);
                }
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext10 = ((ActivityBuyStock_TypedDataContext1_ForReadOnly)(cachedCompiledDataContext[0]));
                return valDataContext10.ValueType___Expr10Get();
            }
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public object InvokeExpression(int expressionId, System.Collections.Generic.IList<System.Activities.Location> locations) {
            if ((this.rootActivity == null)) {
                this.rootActivity = this;
            }
            if ((expressionId == 0)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext0 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext0.ValueType___Expr0Get();
            }
            if ((expressionId == 1)) {
                ActivityBuyStock_TypedDataContext1 refDataContext1 = new ActivityBuyStock_TypedDataContext1(locations, true);
                return refDataContext1.GetLocation<double>(refDataContext1.ValueType___Expr1Get, refDataContext1.ValueType___Expr1Set);
            }
            if ((expressionId == 2)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext2 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext2.ValueType___Expr2Get();
            }
            if ((expressionId == 3)) {
                ActivityBuyStock_TypedDataContext1 refDataContext3 = new ActivityBuyStock_TypedDataContext1(locations, true);
                return refDataContext3.GetLocation<bool>(refDataContext3.ValueType___Expr3Get, refDataContext3.ValueType___Expr3Set);
            }
            if ((expressionId == 4)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext4 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext4.ValueType___Expr4Get();
            }
            if ((expressionId == 5)) {
                ActivityBuyStock_TypedDataContext1 refDataContext5 = new ActivityBuyStock_TypedDataContext1(locations, true);
                return refDataContext5.GetLocation<bool>(refDataContext5.ValueType___Expr5Get, refDataContext5.ValueType___Expr5Set);
            }
            if ((expressionId == 6)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext6 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext6.ValueType___Expr6Get();
            }
            if ((expressionId == 7)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext7 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext7.ValueType___Expr7Get();
            }
            if ((expressionId == 8)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext8 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext8.ValueType___Expr8Get();
            }
            if ((expressionId == 9)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext9 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext9.ValueType___Expr9Get();
            }
            if ((expressionId == 10)) {
                ActivityBuyStock_TypedDataContext1_ForReadOnly valDataContext10 = new ActivityBuyStock_TypedDataContext1_ForReadOnly(locations, true);
                return valDataContext10.ValueType___Expr10Get();
            }
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public bool CanExecuteExpression(string expressionText, bool isReference, System.Collections.Generic.IList<System.Activities.LocationReference> locations, out int expressionId) {
            if (((isReference == false) 
                        && ((expressionText == "totalAmountRequiredForPurchase_InArg") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 0;
                return true;
            }
            if (((isReference == true) 
                        && ((expressionText == "totalAmountRequiredForPurchase_OutArg") 
                        && (ActivityBuyStock_TypedDataContext1.Validate(locations, true, 0) == true)))) {
                expressionId = 1;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "BankConfirmation_InArg") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 2;
                return true;
            }
            if (((isReference == true) 
                        && ((expressionText == "BankConfirmation_OutArg") 
                        && (ActivityBuyStock_TypedDataContext1.Validate(locations, true, 0) == true)))) {
                expressionId = 3;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "BrokerConfirmation_InArg") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 4;
                return true;
            }
            if (((isReference == true) 
                        && ((expressionText == "BrokerConfirmation_OutArg") 
                        && (ActivityBuyStock_TypedDataContext1.Validate(locations, true, 0) == true)))) {
                expressionId = 5;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "BankConfirmation_InArg.Equals(true)") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 6;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "BrokerConfirmation_InArg.Equals(true)") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 7;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "new Exception()") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 8;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "new Exception()") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 9;
                return true;
            }
            if (((isReference == false) 
                        && ((expressionText == "new Exception()") 
                        && (ActivityBuyStock_TypedDataContext1_ForReadOnly.Validate(locations, true, 0) == true)))) {
                expressionId = 10;
                return true;
            }
            expressionId = -1;
            return false;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public System.Collections.Generic.IList<string> GetRequiredLocations(int expressionId) {
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        public System.Linq.Expressions.Expression GetExpressionTreeForExpression(int expressionId, System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) {
            if ((expressionId == 0)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr0GetTree();
            }
            if ((expressionId == 1)) {
                return new ActivityBuyStock_TypedDataContext1(locationReferences).@__Expr1GetTree();
            }
            if ((expressionId == 2)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr2GetTree();
            }
            if ((expressionId == 3)) {
                return new ActivityBuyStock_TypedDataContext1(locationReferences).@__Expr3GetTree();
            }
            if ((expressionId == 4)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr4GetTree();
            }
            if ((expressionId == 5)) {
                return new ActivityBuyStock_TypedDataContext1(locationReferences).@__Expr5GetTree();
            }
            if ((expressionId == 6)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr6GetTree();
            }
            if ((expressionId == 7)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr7GetTree();
            }
            if ((expressionId == 8)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr8GetTree();
            }
            if ((expressionId == 9)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr9GetTree();
            }
            if ((expressionId == 10)) {
                return new ActivityBuyStock_TypedDataContext1_ForReadOnly(locationReferences).@__Expr10GetTree();
            }
            return null;
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityBuyStock_TypedDataContext0 : System.Activities.XamlIntegration.CompiledDataContext {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            public ActivityBuyStock_TypedDataContext0(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext0(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext0(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            internal static object GetDataContextActivitiesHelper(System.Activities.Activity compiledRoot, bool forImplementation) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetDataContextActivities(compiledRoot, forImplementation);
            }
            
            internal static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
            }
            
            public static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 0))) {
                    return false;
                }
                expectedLocationsCount = 0;
                return true;
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityBuyStock_TypedDataContext0_ForReadOnly : System.Activities.XamlIntegration.CompiledDataContext {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            public ActivityBuyStock_TypedDataContext0_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext0_ForReadOnly(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext0_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            internal static object GetDataContextActivitiesHelper(System.Activities.Activity compiledRoot, bool forImplementation) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetDataContextActivities(compiledRoot, forImplementation);
            }
            
            internal static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
            }
            
            public static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 0))) {
                    return false;
                }
                expectedLocationsCount = 0;
                return true;
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityBuyStock_TypedDataContext1 : ActivityBuyStock_TypedDataContext0 {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            protected bool BrokerConfirmation_InArg;
            
            protected double totalAmountRequiredForPurchase_OutArg;
            
            protected double totalAmountRequiredForPurchase_InArg;
            
            protected bool BankConfirmation_InArg;
            
            protected bool BrokerConfirmation_OutArg;
            
            protected bool BankConfirmation_OutArg;
            
            public ActivityBuyStock_TypedDataContext1(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext1(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext1(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            internal new static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public new virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
                base.SetLocationsOffset(locationsOffset);
            }
            
            internal System.Linq.Expressions.Expression @__Expr1GetTree() {
                
                #line 105 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<double>> expression = () => 
                  totalAmountRequiredForPurchase_OutArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public double @__Expr1Get() {
                
                #line 105 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                  totalAmountRequiredForPurchase_OutArg;
                
                #line default
                #line hidden
            }
            
            public double ValueType___Expr1Get() {
                this.GetValueTypeValues();
                return this.@__Expr1Get();
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public void @__Expr1Set(double value) {
                
                #line 105 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                
                  totalAmountRequiredForPurchase_OutArg = value;
                
                #line default
                #line hidden
            }
            
            public void ValueType___Expr1Set(double value) {
                this.GetValueTypeValues();
                this.@__Expr1Set(value);
                this.SetValueTypeValues();
            }
            
            internal System.Linq.Expressions.Expression @__Expr3GetTree() {
                
                #line 119 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<bool>> expression = () => 
                    BankConfirmation_OutArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public bool @__Expr3Get() {
                
                #line 119 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                    BankConfirmation_OutArg;
                
                #line default
                #line hidden
            }
            
            public bool ValueType___Expr3Get() {
                this.GetValueTypeValues();
                return this.@__Expr3Get();
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public void @__Expr3Set(bool value) {
                
                #line 119 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                
                    BankConfirmation_OutArg = value;
                
                #line default
                #line hidden
            }
            
            public void ValueType___Expr3Set(bool value) {
                this.GetValueTypeValues();
                this.@__Expr3Set(value);
                this.SetValueTypeValues();
            }
            
            internal System.Linq.Expressions.Expression @__Expr5GetTree() {
                
                #line 132 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<bool>> expression = () => 
                    BrokerConfirmation_OutArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public bool @__Expr5Get() {
                
                #line 132 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                    BrokerConfirmation_OutArg;
                
                #line default
                #line hidden
            }
            
            public bool ValueType___Expr5Get() {
                this.GetValueTypeValues();
                return this.@__Expr5Get();
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public void @__Expr5Set(bool value) {
                
                #line 132 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                
                    BrokerConfirmation_OutArg = value;
                
                #line default
                #line hidden
            }
            
            public void ValueType___Expr5Set(bool value) {
                this.GetValueTypeValues();
                this.@__Expr5Set(value);
                this.SetValueTypeValues();
            }
            
            protected override void GetValueTypeValues() {
                this.BrokerConfirmation_InArg = ((bool)(this.GetVariableValue((0 + locationsOffset))));
                this.totalAmountRequiredForPurchase_OutArg = ((double)(this.GetVariableValue((1 + locationsOffset))));
                this.totalAmountRequiredForPurchase_InArg = ((double)(this.GetVariableValue((2 + locationsOffset))));
                this.BankConfirmation_InArg = ((bool)(this.GetVariableValue((3 + locationsOffset))));
                this.BrokerConfirmation_OutArg = ((bool)(this.GetVariableValue((4 + locationsOffset))));
                this.BankConfirmation_OutArg = ((bool)(this.GetVariableValue((5 + locationsOffset))));
                base.GetValueTypeValues();
            }
            
            protected override void SetValueTypeValues() {
                this.SetVariableValue((0 + locationsOffset), this.BrokerConfirmation_InArg);
                this.SetVariableValue((1 + locationsOffset), this.totalAmountRequiredForPurchase_OutArg);
                this.SetVariableValue((2 + locationsOffset), this.totalAmountRequiredForPurchase_InArg);
                this.SetVariableValue((3 + locationsOffset), this.BankConfirmation_InArg);
                this.SetVariableValue((4 + locationsOffset), this.BrokerConfirmation_OutArg);
                this.SetVariableValue((5 + locationsOffset), this.BankConfirmation_OutArg);
                base.SetValueTypeValues();
            }
            
            public new static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 6))) {
                    return false;
                }
                if ((validateLocationCount == true)) {
                    offset = (locationReferences.Count - 6);
                }
                expectedLocationsCount = 6;
                if (((locationReferences[(offset + 0)].Name != "BrokerConfirmation_InArg") 
                            || (locationReferences[(offset + 0)].Type != typeof(bool)))) {
                    return false;
                }
                if (((locationReferences[(offset + 1)].Name != "totalAmountRequiredForPurchase_OutArg") 
                            || (locationReferences[(offset + 1)].Type != typeof(double)))) {
                    return false;
                }
                if (((locationReferences[(offset + 2)].Name != "totalAmountRequiredForPurchase_InArg") 
                            || (locationReferences[(offset + 2)].Type != typeof(double)))) {
                    return false;
                }
                if (((locationReferences[(offset + 3)].Name != "BankConfirmation_InArg") 
                            || (locationReferences[(offset + 3)].Type != typeof(bool)))) {
                    return false;
                }
                if (((locationReferences[(offset + 4)].Name != "BrokerConfirmation_OutArg") 
                            || (locationReferences[(offset + 4)].Type != typeof(bool)))) {
                    return false;
                }
                if (((locationReferences[(offset + 5)].Name != "BankConfirmation_OutArg") 
                            || (locationReferences[(offset + 5)].Type != typeof(bool)))) {
                    return false;
                }
                return ActivityBuyStock_TypedDataContext0.Validate(locationReferences, false, offset);
            }
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Activities", "4.0.0.0")]
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        private class ActivityBuyStock_TypedDataContext1_ForReadOnly : ActivityBuyStock_TypedDataContext0_ForReadOnly {
            
            private int locationsOffset;
            
            private static int expectedLocationsCount;
            
            protected bool BrokerConfirmation_InArg;
            
            protected double totalAmountRequiredForPurchase_OutArg;
            
            protected double totalAmountRequiredForPurchase_InArg;
            
            protected bool BankConfirmation_InArg;
            
            protected bool BrokerConfirmation_OutArg;
            
            protected bool BankConfirmation_OutArg;
            
            public ActivityBuyStock_TypedDataContext1_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locations, System.Activities.ActivityContext activityContext, bool computelocationsOffset) : 
                    base(locations, activityContext, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext1_ForReadOnly(System.Collections.Generic.IList<System.Activities.Location> locations, bool computelocationsOffset) : 
                    base(locations, false) {
                if ((computelocationsOffset == true)) {
                    this.SetLocationsOffset((locations.Count - expectedLocationsCount));
                }
            }
            
            public ActivityBuyStock_TypedDataContext1_ForReadOnly(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences) : 
                    base(locationReferences) {
            }
            
            internal new static System.Activities.XamlIntegration.CompiledDataContext[] GetCompiledDataContextCacheHelper(object dataContextActivities, System.Activities.ActivityContext activityContext, System.Activities.Activity compiledRoot, bool forImplementation, int compiledDataContextCount) {
                return System.Activities.XamlIntegration.CompiledDataContext.GetCompiledDataContextCache(dataContextActivities, activityContext, compiledRoot, forImplementation, compiledDataContextCount);
            }
            
            public new virtual void SetLocationsOffset(int locationsOffsetValue) {
                locationsOffset = locationsOffsetValue;
                base.SetLocationsOffset(locationsOffset);
            }
            
            internal System.Linq.Expressions.Expression @__Expr0GetTree() {
                
                #line 110 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<double>> expression = () => 
                  totalAmountRequiredForPurchase_InArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public double @__Expr0Get() {
                
                #line 110 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                  totalAmountRequiredForPurchase_InArg;
                
                #line default
                #line hidden
            }
            
            public double ValueType___Expr0Get() {
                this.GetValueTypeValues();
                return this.@__Expr0Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr2GetTree() {
                
                #line 124 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<bool>> expression = () => 
                    BankConfirmation_InArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public bool @__Expr2Get() {
                
                #line 124 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                    BankConfirmation_InArg;
                
                #line default
                #line hidden
            }
            
            public bool ValueType___Expr2Get() {
                this.GetValueTypeValues();
                return this.@__Expr2Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr4GetTree() {
                
                #line 137 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<bool>> expression = () => 
                    BrokerConfirmation_InArg;
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public bool @__Expr4Get() {
                
                #line 137 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                    BrokerConfirmation_InArg;
                
                #line default
                #line hidden
            }
            
            public bool ValueType___Expr4Get() {
                this.GetValueTypeValues();
                return this.@__Expr4Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr6GetTree() {
                
                #line 149 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<bool>> expression = () => 
                BankConfirmation_InArg.Equals(true);
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public bool @__Expr6Get() {
                
                #line 149 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                BankConfirmation_InArg.Equals(true);
                
                #line default
                #line hidden
            }
            
            public bool ValueType___Expr6Get() {
                this.GetValueTypeValues();
                return this.@__Expr6Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr7GetTree() {
                
                #line 156 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<bool>> expression = () => 
                    BrokerConfirmation_InArg.Equals(true);
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public bool @__Expr7Get() {
                
                #line 156 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                    BrokerConfirmation_InArg.Equals(true);
                
                #line default
                #line hidden
            }
            
            public bool ValueType___Expr7Get() {
                this.GetValueTypeValues();
                return this.@__Expr7Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr8GetTree() {
                
                #line 171 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<System.Exception>> expression = () => 
                          new Exception();
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public System.Exception @__Expr8Get() {
                
                #line 171 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                          new Exception();
                
                #line default
                #line hidden
            }
            
            public System.Exception ValueType___Expr8Get() {
                this.GetValueTypeValues();
                return this.@__Expr8Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr9GetTree() {
                
                #line 186 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<System.Exception>> expression = () => 
                    new Exception();
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public System.Exception @__Expr9Get() {
                
                #line 186 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                    new Exception();
                
                #line default
                #line hidden
            }
            
            public System.Exception ValueType___Expr9Get() {
                this.GetValueTypeValues();
                return this.@__Expr9Get();
            }
            
            internal System.Linq.Expressions.Expression @__Expr10GetTree() {
                
                #line 201 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                System.Linq.Expressions.Expression<System.Func<System.Exception>> expression = () => 
                  new Exception();
                
                #line default
                #line hidden
                return base.RewriteExpressionTree(expression);
            }
            
            [System.Diagnostics.DebuggerHiddenAttribute()]
            public System.Exception @__Expr10Get() {
                
                #line 201 "D:\0.ISEL\DISCIPLINAS\COS1415\AZURECOS1415TRABFINAL\WEBROLEMYBROKER\WF\ACTIVITYBUYSTOCK.XAML"
                return 
                  new Exception();
                
                #line default
                #line hidden
            }
            
            public System.Exception ValueType___Expr10Get() {
                this.GetValueTypeValues();
                return this.@__Expr10Get();
            }
            
            protected override void GetValueTypeValues() {
                this.BrokerConfirmation_InArg = ((bool)(this.GetVariableValue((0 + locationsOffset))));
                this.totalAmountRequiredForPurchase_OutArg = ((double)(this.GetVariableValue((1 + locationsOffset))));
                this.totalAmountRequiredForPurchase_InArg = ((double)(this.GetVariableValue((2 + locationsOffset))));
                this.BankConfirmation_InArg = ((bool)(this.GetVariableValue((3 + locationsOffset))));
                this.BrokerConfirmation_OutArg = ((bool)(this.GetVariableValue((4 + locationsOffset))));
                this.BankConfirmation_OutArg = ((bool)(this.GetVariableValue((5 + locationsOffset))));
                base.GetValueTypeValues();
            }
            
            public new static bool Validate(System.Collections.Generic.IList<System.Activities.LocationReference> locationReferences, bool validateLocationCount, int offset) {
                if (((validateLocationCount == true) 
                            && (locationReferences.Count < 6))) {
                    return false;
                }
                if ((validateLocationCount == true)) {
                    offset = (locationReferences.Count - 6);
                }
                expectedLocationsCount = 6;
                if (((locationReferences[(offset + 0)].Name != "BrokerConfirmation_InArg") 
                            || (locationReferences[(offset + 0)].Type != typeof(bool)))) {
                    return false;
                }
                if (((locationReferences[(offset + 1)].Name != "totalAmountRequiredForPurchase_OutArg") 
                            || (locationReferences[(offset + 1)].Type != typeof(double)))) {
                    return false;
                }
                if (((locationReferences[(offset + 2)].Name != "totalAmountRequiredForPurchase_InArg") 
                            || (locationReferences[(offset + 2)].Type != typeof(double)))) {
                    return false;
                }
                if (((locationReferences[(offset + 3)].Name != "BankConfirmation_InArg") 
                            || (locationReferences[(offset + 3)].Type != typeof(bool)))) {
                    return false;
                }
                if (((locationReferences[(offset + 4)].Name != "BrokerConfirmation_OutArg") 
                            || (locationReferences[(offset + 4)].Type != typeof(bool)))) {
                    return false;
                }
                if (((locationReferences[(offset + 5)].Name != "BankConfirmation_OutArg") 
                            || (locationReferences[(offset + 5)].Type != typeof(bool)))) {
                    return false;
                }
                return ActivityBuyStock_TypedDataContext0_ForReadOnly.Validate(locationReferences, false, offset);
            }
        }
    }
}
