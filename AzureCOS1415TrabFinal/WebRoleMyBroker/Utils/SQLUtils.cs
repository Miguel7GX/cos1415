﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebRoleMyBroker.Utils
{
    public class SQLUtils
    {



        // Método de conexão à BD - Verifica qual é o proximo número operação disponível
        public static int SQLCheckNextOperationNumber(string command)
        {

            int nextAvailableAccountNr = 0;

            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\App_Data\ClientOperations.mdf;Integrated Security=True"))
            {
                try
                {
                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);
                    nextAvailableAccountNr = (int)myCommand.ExecuteScalar();
                }

                catch (Exception ex)
                {
                    Console.WriteLine("Excepção: {0}", ex.ToString());
                }

                finally
                {

                    conn.Close();
                    conn.Dispose();
                }
            }
            return nextAvailableAccountNr + 1;
        }


        //Método de conexão à BD - Criar registo na BD
        public static void SQLRegisterOperation(string command, string operationID, string clientID, string clientBankAccount, string stockQuote, int stockAmount, double operationCost, string opType)
        {

            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\App_Data\ClientOperations.mdf;Integrated Security=True"))
            {
                try
                {

                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);


                    myCommand.Parameters.Add("@OperationID", System.Data.SqlDbType.VarChar).Value = operationID;
                    myCommand.Parameters.Add("@userID", System.Data.SqlDbType.VarChar).Value = clientID;
                    myCommand.Parameters.Add("@clientBankAccount", System.Data.SqlDbType.VarChar).Value = clientBankAccount;
                    myCommand.Parameters.Add("@stockQuote", System.Data.SqlDbType.VarChar).Value = stockQuote;
                    myCommand.Parameters.Add("@stockAmount", System.Data.SqlDbType.Int).Value = stockAmount;
                    myCommand.Parameters.Add("@operationCost", System.Data.SqlDbType.Float).Value = operationCost;
                    myCommand.Parameters.Add("@opType", System.Data.SqlDbType.VarChar).Value = opType;


                    myCommand.ExecuteNonQuery();

                    myCommand.Dispose();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

        }


        //Método de conexão à BD - Criar registo na BD
        public static void SQLRegisterOperationClientWallet(string command, string operationID, string clientID, string clientBankAccount, string stockQuote, int stockAmount, double operationCost)
        {

            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\App_Data\ClientOperations.mdf;Integrated Security=True;"))
            {
                try
                {

                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);


                    myCommand.Parameters.Add("@OperationID", System.Data.SqlDbType.VarChar).Value = operationID;
                    myCommand.Parameters.Add("@userID", System.Data.SqlDbType.VarChar).Value = clientID;
                    myCommand.Parameters.Add("@clientBankAccount", System.Data.SqlDbType.VarChar).Value = clientBankAccount;
                    myCommand.Parameters.Add("@stockQuote", System.Data.SqlDbType.VarChar).Value = stockQuote;
                    myCommand.Parameters.Add("@stockAmount", System.Data.SqlDbType.Int).Value = stockAmount;
                    myCommand.Parameters.Add("@operationCost", System.Data.SqlDbType.Float).Value = operationCost;


                    myCommand.ExecuteNonQuery();

                    myCommand.Dispose();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

        }

        //Método de conexão à BD - Criar registo na BD
        public static void DeleteOperation(string command, string operationID)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\App_Data\ClientOperations.mdf;Integrated Security=True"))
            {
                try
                {
                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);

                    myCommand.Parameters.Add("@OperationID", System.Data.SqlDbType.VarChar).Value = operationID;               

                    myCommand.ExecuteNonQuery();
                    myCommand.Dispose();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

        }

     


        //Método de conexão à BD - Executa DataReader
             public static string SQLselectCommandClientWallet(string command, string ClientID, string Stock)
             {
            string opID = "";
            SqlDataReader myReader;

            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\App_Data\ClientOperations.mdf;Integrated Security=True"))
            {
                try
                {
                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);

                    myCommand.Parameters.Add("@userID", System.Data.SqlDbType.VarChar).Value = ClientID;
                    myCommand.Parameters.Add("@stock", System.Data.SqlDbType.VarChar).Value = Stock;
                 
                    myReader = myCommand.ExecuteReader();
                   
                    while (myReader.Read())
                    {                    
                        opID = (myReader["OperationID"].ToString());
                    }

                    conn.Close();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Close();
                    conn.Dispose();
                }

            }
            return opID.Trim();
        }



        //Método de conexão à BD - Executa DataReader
        public static void SQLUpdateCommand(string command)
        {

            using (SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\0.ISEL\Disciplinas\COS1415\AzureCOS1415TrabFinal\WebRoleMyBroker\App_Data\ClientOperations.mdf;Integrated Security=True"))
            {
                try
                {
                    conn.Open();
                    SqlCommand myCommand = new SqlCommand(command, conn);
                    myCommand.ExecuteNonQuery();
                    conn.Close();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                finally
                {
                    conn.Dispose();
                }
            }


        }
    }
}