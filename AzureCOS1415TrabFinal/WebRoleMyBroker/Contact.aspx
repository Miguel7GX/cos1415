﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebRoleMyBroker.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>My Broker Trading Inc</h3>
     <address>
        1585 Broadway <br />
        New York, NY 10036</address>

    <address>
        <strong>Support:</strong>   <a href="mailto:Support@example.com">Support@<a href="mailto:Marketing@example.com">mybroker</a>.com</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@mybroker.com</a>
    </address>
</asp:Content>
